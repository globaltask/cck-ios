//
//  main.m
//  CCK iOS
//
//  Created by Nicolas Vidal on 6/16/15.
//  Copyright (c) 2015 Nicolas Vidal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
